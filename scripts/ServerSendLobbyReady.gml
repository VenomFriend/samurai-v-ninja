///ServerSendLobbyReady(socket, isReady)
var playerSocket = argument[0];
var playerIsReady = argument[1];


var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_LOBBY_READY);
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);
buffer_write(global.bufferServerWrite, buffer_bool, playerIsReady);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if(thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
