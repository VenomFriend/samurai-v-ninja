///CheckAttackProjectile();
// Called by script AttackPlayer, which is called by objPlayer

// Server checks if an attack hit a projectile, and destroys it
if(global.isHost) {
    if(place_meeting(x, y, objThrowingWeapon)) {
        var projectile = instance_place(x, y, objThrowingWeapon);
        if(projectile != noone) {
            with(projectile) {
                instance_destroy();
            }
        }
    }
}
