if (file_exists("options.ini")) {
    ini_open("options.ini");
    if (ini_section_exists("kb_controls")) {
        global.KEY_UP = ini_read_real("kb_controls", "up", vk_up);
        global.KEY_DOWN = ini_read_real("kb_controls", "down", vk_down);
        global.KEY_LEFT = ini_read_real("kb_controls", "left", vk_left);
        global.KEY_RIGHT = ini_read_real("kb_controls", "right", vk_right);
        global.KEY_JUMP = ini_read_real("kb_controls", "jump", vk_space);
        global.KEY_ATTACK = ini_read_real("kb_controls", "attack", ord('A'));
        global.KEY_SHOOT = ini_read_real("kb_controls", "shoot", ord('D'));
        global.KEY_TELEPORT = ini_read_real("kb_controls", "teleport", ord('S'));
    }
    if(ini_section_exists("connect")){
        global.ipAddress = ini_read_string("connect", "ip", "127.0.0.1");
        global.port = ini_read_real("connect", "port", 8000);
    }
    if(ini_section_exists("character")){
        global.char = ini_read_real("character", "char", 1);
    }
    ini_close();
}
