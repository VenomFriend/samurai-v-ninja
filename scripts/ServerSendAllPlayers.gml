///ServerSendAllPlayers(socket)
var playerSocket = argument[0];


//The number of sockets = the number of players connected
var playerCount = ds_list_size(global.socketList);
var numberOfPlayers = playerCount + 1; // the server is also a player
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u16, numberOfPlayers);


// Send the server's own player, since you can only host and play at the same time right now
    
var serverPlayer = instance_find(objPlayer, 0);
buffer_write(global.bufferServerWrite, buffer_u32, serverPlayer.playerID);  //Remember, kids, your ID is your socket, not your player.id!
buffer_write(global.bufferServerWrite, buffer_f32, serverPlayer.x);
buffer_write(global.bufferServerWrite, buffer_f32, serverPlayer.y);
buffer_write(global.bufferServerWrite, buffer_s8, serverPlayer.facing);
buffer_write(global.bufferServerWrite, buffer_u8, serverPlayer.character);
buffer_write(global.bufferServerWrite, buffer_string, serverPlayer.name);
buffer_write(global.bufferServerWrite, buffer_bool, serverPlayer.isReady);

//Write a really big packet with everybody information
for(var i = 0; i < playerCount; i++) {
    var otherPlayerSocket = ds_list_find_value(global.socketList, i);
    var otherPlayer = ds_map_find_value(global.Clients, otherPlayerSocket);
    
    
    //I really don't need to send to the player his own information, right?
    if (otherPlayerSocket != playerSocket) {
        buffer_write(global.bufferServerWrite, buffer_u32, otherPlayerSocket);  //Remember, kids, your ID is your socket, not your player.id!
        buffer_write(global.bufferServerWrite, buffer_f32, otherPlayer.x);
        buffer_write(global.bufferServerWrite, buffer_f32, otherPlayer.y);
        buffer_write(global.bufferServerWrite, buffer_s8, otherPlayer.facing);
        buffer_write(global.bufferServerWrite, buffer_u8, otherPlayer.character);
        buffer_write(global.bufferServerWrite, buffer_string, otherPlayer.name);
        buffer_write(global.bufferServerWrite, buffer_bool, otherPlayer.isReady);
    }
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
