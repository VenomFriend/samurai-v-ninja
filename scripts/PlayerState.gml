///PlayerState()
// Called by objPlayer
enum state {
    idle = 0,
    moving = 1,
    jumping = 2,
    falling = 3,
    attacking = 4,
    holdingAttack = 5,
    throwing = 6,
    wallSliding = 7,
    dead = 8
}
