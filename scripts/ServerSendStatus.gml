///ServerSendStatus(playerID, player)
var otherPlayerID = argument[0];
var player = argument[1];

var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_STATUS);
buffer_write(global.bufferServerWrite, buffer_u32, otherPlayerID);
with(player) {
    buffer_write(global.bufferServerWrite, buffer_bool, canTeleport);
    buffer_write(global.bufferServerWrite, buffer_bool, canAttack);
    buffer_write(global.bufferServerWrite, buffer_bool, canShoot);
    buffer_write(global.bufferServerWrite, buffer_bool, canFall);
    buffer_write(global.bufferServerWrite, buffer_bool, climbPower);
    buffer_write(global.bufferServerWrite, buffer_bool, wallWalking);
    buffer_write(global.bufferServerWrite, buffer_bool, ceilingWalking);
    buffer_write(global.bufferServerWrite, buffer_s8, wallDirection);
    buffer_write(global.bufferServerWrite, buffer_bool, jumpPower);
    buffer_write(global.bufferServerWrite, buffer_bool, doubleJumped);
}

for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    if(thisSocket != otherPlayerID) {
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
    }
}
