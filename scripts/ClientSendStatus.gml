///ClientSendStatus(player)
//For now, just say if you can or can't teleport and if you can or can't attack
//May put more status here in the future
//(FUTURE) DEAR GOD, WHAT HAVE I DONE?

var player = argument[0];

with(player) {
    buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
    buffer_write(global.bufferClientWrite, buffer_u8, MSG_STATUS);
    buffer_write(global.bufferClientWrite, buffer_bool, canTeleport);
    buffer_write(global.bufferClientWrite, buffer_bool, canAttack);
    buffer_write(global.bufferClientWrite, buffer_bool, canShoot);
    buffer_write(global.bufferClientWrite, buffer_bool, canFall);
    buffer_write(global.bufferClientWrite, buffer_bool, climbPower);
    buffer_write(global.bufferClientWrite, buffer_bool, wallWalking);
    buffer_write(global.bufferClientWrite, buffer_bool, ceilingWalking);
    buffer_write(global.bufferClientWrite, buffer_s8, wallDirection);
    buffer_write(global.bufferClientWrite, buffer_bool, jumpPower);
    buffer_write(global.bufferClientWrite, buffer_bool, doubleJumped);
}

network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
