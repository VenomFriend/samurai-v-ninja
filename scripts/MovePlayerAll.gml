///MovePlayerWW()
// Called by objPlayer

// Some checkings, just to make sure
if(ceilingWalking) {
    wallWalking = false;
    wallDirection = 1;
}
if(wallWalking) {
    ceilingWalking = false;
}

var delta = 60/1000000 * delta_time;

if(wallWalking or ceilingWalking) {
    vspd = 0;
} else {
    if (vspd < vspdMax) {
        vspd += grav;
        if (vspd > vspdMax) {
            vspd = vspdMax;
        }
    }
}

if(wallWalking) {
    var movement = -keyUp + keyDown;
} else {
    var movement = -keyLeft + keyRight;
}
// keyLeft is either 1(active) or 0
// keyRight is either 1(active) or 0
//so if movement = -1, it's going left, if movement = 0, it's not moving and if movement = 1, it's going right

if(!wallWalking and !ceilingWalking) {
    if(movement == 1) {
        facing = 1; // facing right
    } else if(movement == -1){
        facing = -1; //facing left
    }
} else {
    if(wallDirection == -1) { // wall on the left
        if(movement == 1) {
            facing = 1; // facing down
        } else if(movement == -1){
            facing = -1; //facing up
        }
    } else if(wallDirection == 1){
        if(movement == 1) { // wall on the right
            facing = -1; // facing down
        } else if(movement == -1){
            facing = 1; //facing up
        }
    }
}

// Double jump

if(playerState == state.jumping or playerState == state.falling) {
    if(keyJumpPressed and jumpPower and !doubleJumped) {
        doubleJumped = true;
        vspd = jumpSpeed;
        playerState = state.jumping;
        effect_create_above(ef_smoke, x+13, y+13, 0.3, c_gray);    
    }
}

if(!wallWalking and !ceilingWalking) {
    if(place_meeting(x, y+1, objCollision)){
        hspd = movement * playerSpeed;
        //If you cannot attack because you already attacked, when you are on the floor you'll be able to
        //attack again. This way, you won't be able to attack twice while on air... I guess :P
        if(!canAttack){
            canAttack = true;
        }
        if(hspd != 0) {
            playerState = state.moving;
        } else {
            playerState = state.idle;
        }
    } else {
        hspd += movement * 1;
        if(abs(hspd) > abs(playerSpeed)){
            hspd = movement * playerSpeed;
        }
    }

    if(keyJumpPressed) {
        // If you are on the floor
        if(place_meeting(x, y+1, objCollision)){ // changed y+vspd to y+1
            audio_play_sound_at(sndJump, x, y, 0, 100, 300, 1, false, 1);
            vspd = jumpSpeed;
            playerState = state.jumping;
        } else if(climbPower == false){
            // If there's a wall on your right
            if(place_meeting(x+1, y, objCollision)){
                wallWalking = false;
                playerAngle = 0;
                audio_play_sound_at(sndJump, x, y, 0, 100, 300, 1, false, 1);
                vspd = jumpSpeed;
                hspd = -playerSpeed;
                facing = sign(hspd);
                playerState = state.jumping;
                //After a wall jump, you can attack again
                if(!canAttack){
                    canAttack = true;
                }
            // If there's a wall on your left
            } else if(place_meeting(x-1, y, objCollision)){
                wallWalking = false;
                playerAngle = 0;
                audio_play_sound_at(sndJump, x, y, 0, 100, 300, 1, false, 1);
                vspd = jumpSpeed;
                hspd = playerSpeed;
                facing = sign(hspd);
                playerState = state.jumping;
                //After a wall jump, you can attack again
                if(!canAttack){
                    canAttack = true;
                }
            }
        }
    }
    if(climbPower) { // If climb power is true and you touch a wall, climb it
        if(place_meeting(x + (hspd * delta), y, objCollision)) {
            while(!place_meeting(x + (sign(hspd)*delta), y, objCollision)){
                x += sign(hspd) * delta;
            }
            wallDirection = sign(hspd);
            if(wallDirection > 0) {
                playerAngle = 90;
                wallDirection = 1;
            } else {
                playerAngle = 270;
                wallDirection = -1;
            }
            hspd = 0;
            vspd = 0;
            wallWalking = true;
            ceilingWalking = false;      
        }
        
        if(place_meeting(x, y + (vspd * delta), objCollision)) {
            while(!place_meeting(x, y + (sign(vspd)*delta), objCollision)){
                y += sign(vspd)*delta;
            }
            if(sign(vspd) < 0){
                wallWalking = false;
                ceilingWalking = true;
                playerAngle = 180;
                wallDirection = 1; // Yeah, we need this so the direction is right... hehe
            }
            vspd = 0;
        }
        if(place_meeting(x, y-1, objCollision)) {
            wallWalking = false;
            ceilingWalking = true;
            playerAngle = 180;
            wallDirection = 1;
            vspd = 0;
        }
    } else { // if climb power is false, slide through the wall
        if (!place_meeting(x, y+1, objCollision)) { //(vspd != 0 and !place_meeting(x, y+1, objCollision)) {
            if(place_meeting(x+1, y, objCollision) or place_meeting(x-1, y, objCollision)){
                playerState = state.wallSliding;
                if( (keyLeft and place_meeting(x-1, y, objCollision)) or (keyRight and place_meeting(x+1, y, objCollision)) ) {
                    if(vspd >= vspdMax/10 ) {
                        vspd = vspdMax/10;
                    }
                }
            } else {
                if(vspd < 0) {
                    playerState = state.jumping;
                } else {
                    playerState = state.falling;
                }
            }
        }
    }
}

if(wallWalking) {
    movement = -keyUp + keyDown;
    vspd = movement * playerSpeed;
    if(place_meeting(x + wallDirection, y, objCollision) and !place_meeting(x, y + vspd, objCollision)){
        if(!canAttack){
            canAttack = true;
        }
        if(vspd != 0) {
            playerState = state.moving;
        } else {
            playerState = state.idle;
        }
    } else {
        wallWalking = false;
        playerAngle = 0;
        if(sign(vspd) < 0){ // if is going up, get on top of the platform
            if(!place_meeting(x + wallDirection * delta * 15, y, objCollision)){
                x += wallDirection*delta*15;
            }
        }
        vspd = 0;
    }
    if(keyJumpPressed){
        wallWalking = false;
        vspd = jumpSpeed;
        if(wallDirection == 1) {
            hspd = -playerSpeed;
        } else {
            hspd = playerSpeed;
        }        
        facing = sign(hspd);
        playerState = state.jumping;
        playerAngle = 0;
        if(!canAttack){
            canAttack = true;
        }
    }
    //y += vspd * delta;
}

if(ceilingWalking) {
    if(place_meeting(x, y-1, objCollision) and !place_meeting(x + sign(hspd), y + 17, objCollision)) {
        hspd = movement * playerSpeed;
        if(!canAttack){
            canAttack = true;
        }
        if(hspd != 0) {
            playerState = state.moving;
        } else {
            playerState = state.idle;
        }
    } else {
        ceilingWalking = false;
        playerAngle = 0;
    }
    if(keyJumpPressed){
        ceilingWalking = false;
        playerAngle = 0;
        vspd = vspdMax;
    }
    //x += hspd * delta;
}


// Reset the double jump
if(playerState != state.jumping and playerState != state.falling) {
    // Only reset if you are either on the floor or touching a wall
    if(place_meeting(x+1, y, objCollision) or place_meeting(x-1, y, objCollision) or place_meeting(x, y+1, objCollision)){
        if(doubleJumped) {
            doubleJumped = false;
        }
    }
}

// Those checkings here might be redundant, but just to be 1000% sure
if(place_meeting(x + (hspd * delta), y, objCollision)) {
    while(!place_meeting(x + (sign(hspd) * delta), y, objCollision)){
        x += (sign(hspd) * delta);
    }
    hspd = 0;
}
if(place_meeting(x, y + (vspd * delta), objCollision)) {
    while(!place_meeting(x, y + (sign(vspd) * delta), objCollision)){
        y += (sign(vspd) * delta);
    }
    vspd = 0;
}

if(place_meeting(x + (hspd * delta), y, objPlayer)) {
    var otherPlayer = instance_place(x + (hspd * delta), y, objPlayer);
    if(otherPlayer != noone and otherPlayer.playerState != state.dead) {
        while(!place_meeting(x + (sign(hspd) * delta), y, objPlayer)){
            x += (sign(hspd) * delta);
        }
        hspd = 0;
    }
}

if(place_meeting(x, y + (vspd * delta), objPlayer)) {
    var otherPlayer = instance_place(x, y + (vspd * delta), objPlayer);
    if(otherPlayer != noone) {
        while(!place_meeting(x, y + (sign(vspd) * delta), objPlayer)){
            y += (sign(vspd) * delta);
        }
        vspd = 0;
        if bbox_bottom < otherPlayer.bbox_top  and (playerState == state.falling or playerState == state.jumping){
            vspd = jumpSpeed;
            playerState = state.jumping;
            if(global.isHost and otherPlayer.playerState != state.dead) {
                otherPlayer.playerState = state.dead;
                if(otherPlayer.isPlayer) { // If is the server's player, set the respawn
                    otherPlayer.alarm[2] = global.deltaMultiplier * delta_time * 3 * 60;
                }
                playerKills++;
                ServerSendScore(playerID, playerKills);
                ServerSendDeath(otherPlayer.playerID, state.dead);
                otherPlayer.ammo = global.ammoDefault;
                ServerSendAmmo(otherPlayer.playerID, otherPlayer.ammo); // resets the player's ammo
            }
        }
    }
}

/// Some checkings to change the animation to jumping/falling if needed... This code is a big mess, I really need to organize it better, so...
/// TODO: Organize the shit of this movement code

// This shit is ugly, but it works
if(playerState != state.wallSliding and !(place_meeting(x, y+1, objCollision)) and !wallWalking and !ceilingWalking) {
    if(vspd < 0) {
        playerState = state.jumping;
    } else {
        playerState = state.falling;
    }
}


// THIS HERE IS IMPORTANT AS HELL!
if(!place_meeting(x + (hspd * delta), y, objCollision) and !place_meeting(x + (hspd * delta), y, objPlayer)) {
    x += (hspd * delta);
}
if(!place_meeting(x, y + (vspd * delta), objCollision) and !place_meeting(x, y + (vspd * delta), objPlayer)) {
    y += (vspd * delta);
}
