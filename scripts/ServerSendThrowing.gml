///ServerSendThrowing(x, y, weapon, xMovement, yMovement, facing, id)

var xAtt = argument[0];
var yAtt = argument[1];
var weapon = argument[2];
var xMovement = argument[3];
var yMovement = argument[4];
var facing = argument[5];
var attackID = argument[6];

//The number of sockets = the number of players connected
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_THROWING);
buffer_write(global.bufferServerWrite, buffer_f32, xAtt);
buffer_write(global.bufferServerWrite, buffer_f32, yAtt);
buffer_write(global.bufferServerWrite, buffer_u16, weapon);
buffer_write(global.bufferServerWrite, buffer_f32, xMovement);
buffer_write(global.bufferServerWrite, buffer_f32, yMovement);
buffer_write(global.bufferServerWrite, buffer_s8, facing);
buffer_write(global.bufferServerWrite, buffer_u32, attackID);

for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
