///ServerSendCreatePlayer(socket, x, y, char, respawnX)
var socket = argument[0];
var playerX = argument[1];
var playerY = argument[2];
var playerChar = argument[3];
var respawnX = argument[4];

// Tell the player who just connected his ID and position
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_ID);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
buffer_write(global.bufferServerWrite, buffer_f32, playerX);
buffer_write(global.bufferServerWrite, buffer_f32, playerY);
buffer_write(global.bufferServerWrite, buffer_u8, playerChar);
buffer_write(global.bufferServerWrite, buffer_u8, respawnX);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
