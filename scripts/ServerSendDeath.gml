///ServerSendDeath(socket, state)
var playerSocket = argument[0];
var otherPlayerState = argument[1];


var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_STATE);
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);
buffer_write(global.bufferServerWrite, buffer_u16, otherPlayerState);

// This message is gonna be send to everybody,
// including the playerSocket guy. 
// The reason is that even him needs to know that he died,
// since now the one who checks is the server.
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
