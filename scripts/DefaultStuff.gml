///DefaultStuff
//Set the default values
global.KEY_LEFT = vk_left;
global.KEY_RIGHT = vk_right;
global.KEY_UP = vk_up;
global.KEY_DOWN = vk_down;
global.KEY_ATTACK = ord('A');
global.KEY_JUMP = vk_space;
global.KEY_TELEPORT = ord('S');
global.KEY_SHOOT = ord('D');
global.ipAddress = "127.0.0.1";
global.port = 8000;
global.char = 1;
global.name = "John Doe";
global.deltaMultiplier = 60/1000000;
global.showName = false;
global.isHost = false;
global.debug = false;
global.showScore = false;

global.charCount = 5; // 5 = 6 players
global.ammoDefault = 5;
global.ammoMax = 9;
