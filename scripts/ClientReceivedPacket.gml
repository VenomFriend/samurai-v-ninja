///ClientReceivedPacket(buffer)
var bufferClientReceived = argument[0]
var msgid = buffer_read(bufferClientReceived, buffer_u8);

switch(msgid) {
    // Create your own player, and set his ID to be his SOCKET on the SERVER
    case MSG_CREATE_ID:
        var playerID = buffer_read(bufferClientReceived, buffer_u32);
        var playerX = buffer_read(bufferClientReceived, buffer_f32);
        var playerY = buffer_read(bufferClientReceived, buffer_f32);
        var myPlayer = instance_create(playerX, playerY, objPlayer);
        var myChar = buffer_read(bufferClientReceived, buffer_u8);
        var myRespawnX = buffer_read(bufferClientReceived, buffer_u8);
        myPlayer.playerID = playerID;        
        myPlayer.isPlayer = true;
        myPlayer.character = myChar; // Set my character sprite to one that was not used yet on the lobby
        myPlayer.name = global.name;
        myPlayer.respawnX = myRespawnX;
        ds_map_add(global.players, playerID, myPlayer); // Save my own character on the MAP of players
        ClientSendCharacter(myPlayer.character, myPlayer.name); // And tell the server
        break;
    // Create other player(s) and set their ID to their SOCKET
    case MSG_CREATE_PLAYER:
        var playerCount = buffer_read(bufferClientReceived, buffer_u16);
        for(var i = 0; i < playerCount; i++) {
            var otherPlayerID = buffer_read(bufferClientReceived, buffer_u32);
            var otherPlayerX = buffer_read(bufferClientReceived, buffer_f32);
            var otherPlayerY = buffer_read(bufferClientReceived, buffer_f32);
            var otherPlayerFacing = buffer_read(bufferClientReceived, buffer_s8);
            var otherPlayerCharacter = buffer_read(bufferClientReceived, buffer_u8);
            var otherPlayerName = buffer_read(bufferClientReceived, buffer_string);
            var otherPlayerIsReady = buffer_read(bufferClientReceived, buffer_bool);
            var otherPlayer = instance_create(otherPlayerX, otherPlayerY, objPlayer);
            //show_debug_message("OTHER PLAYER NAME RECEIVED = " + string(otherPlayerName));
            //show_debug_message("OTHER PLAYER ID RECEIVED = " + string(otherPlayerID));
            
            //I'm not using the "with() {}" because, for some reason, it was not setting
            //the playerID correctly... really strange bug
            otherPlayer.playerID = otherPlayerID;
            otherPlayer.facing = otherPlayerFacing;
            otherPlayer.character = otherPlayerCharacter;
            otherPlayer.name = otherPlayerName;
            otherPlayer.isReady = otherPlayerIsReady;
            
            //show_debug_message("OTHER PLAYER ID SET = " + string(otherPlayer.playerID));
            //show_debug_message("OTHER PLAYER NAME SET = " + string(otherPlayer.name));
            ds_map_add(global.players, otherPlayerID, otherPlayer); // Add the player to the MAP of players, so I can remove him later more easily
        }
        break;
    // Receive the PRESSED KEY from the player
    case MSG_KEY:
        var playerID = buffer_read(bufferClientReceived, buffer_u32);
        var key = buffer_read(bufferClientReceived, buffer_u8);
        ControlOtherPlayer(playerID, key);
        break;
    // Received the player position
    case MSG_POSITION:
        var playerID = buffer_read(bufferClientReceived, buffer_u32);
        var playerX = buffer_read(bufferClientReceived, buffer_f32);
        var playerY = buffer_read(bufferClientReceived, buffer_f32);
        var playerFacing = buffer_read(bufferClientReceived, buffer_s8);
        var playerAngle = buffer_read(bufferClientReceived, buffer_s16);
        UpdateOtherPlayer(playerID, playerX, playerY, playerFacing, playerAngle);
        break;
    // Removes the player who just disconnected
    case MSG_DISCONNECT_PLAYER:
        thisPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        if (!is_undefined(ds_map_find_value(global.players, thisPlayerID))){
            var inst = ds_map_find_value(global.players, thisPlayerID);
            ds_map_delete(global.players, thisPlayerID);
            with (inst) {
                instance_destroy();
            }
        }
        break;
    // Change the player state(like to dead, or attacking, etc)
    case MSG_STATE:
        var playerID = buffer_read(bufferClientReceived, buffer_u32);
        var playerState = buffer_read(bufferClientReceived, buffer_u16);
        ChangePlayerState(playerID, playerState);
        break;
    // Change some player status(like canTeleport)
    case MSG_STATUS:
        var otherPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var otherCanTeleport = buffer_read(bufferClientReceived, buffer_bool);
        var otherCanAttack = buffer_read(bufferClientReceived, buffer_bool);
        var otherCanShoot = buffer_read(bufferClientReceived, buffer_bool);
        var otherCanFall = buffer_read(bufferClientReceived, buffer_bool);
        var otherClimbPower = buffer_read(bufferClientReceived, buffer_bool);
        var otherWallWalking = buffer_read(bufferClientReceived, buffer_bool);
        var otherCeilingWalking = buffer_read(bufferClientReceived, buffer_bool);
        var otherWallDirection = buffer_read(bufferClientReceived, buffer_s8);
        var otherJumpPower = buffer_read(bufferClientReceived, buffer_bool);
        var otherDoubleJumped = buffer_read(bufferClientReceived, buffer_bool);
        ChangePlayerStatus(otherPlayerID, otherCanTeleport, otherCanAttack, otherCanShoot, otherCanFall, otherClimbPower, otherWallWalking, otherCeilingWalking, otherWallDirection, otherJumpPower, otherDoubleJumped);
        break;
    // Change some player character
    case MSG_CHARACTER:
        var playerID = buffer_read(bufferClientReceived, buffer_u32);
        var playerCharacter = buffer_read(bufferClientReceived, buffer_u8);
        var playerName = buffer_read(bufferClientReceived, buffer_string);
        ChangePlayerCharacter(playerID, playerCharacter, playerName);
        break;
    // Displays an error msg and restarts the game
    case MSG_ERROR:
        var msg = buffer_read(bufferClientReceived, buffer_string);
        show_message(msg);
        game_restart();
        break;
    // Change the map and start the game
    case MSG_START:
        var map = buffer_read(bufferClientReceived, buffer_u32);
        var mapX = buffer_read(bufferClientReceived, buffer_f32);
        var mapY = buffer_read(bufferClientReceived, buffer_f32);
        room_goto(map);
        var player = instance_find(objPlayer, 0);
        with(player) {
            x = mapX;
            y = mapY;
            respawnX = mapX;
            respawnY = mapY;
            alarm[1] = 1;
        }
        break;
    // Create the throwing attack
    case MSG_CREATE_THROWING:
        var xAtt = buffer_read(bufferClientReceived, buffer_f32);
        var yAtt = buffer_read(bufferClientReceived, buffer_f32);
        var tWeapon = buffer_read(bufferClientReceived, buffer_u16);
        var xMove = buffer_read(bufferClientReceived, buffer_f32);
        var yMove = buffer_read(bufferClientReceived, buffer_f32);
        var tFacing = buffer_read(bufferClientReceived, buffer_s8);
        var tID = buffer_read(bufferClientReceived, buffer_u32);
        var throwing = instance_create(xAtt, yAtt, objThrowingWeapon);
        audio_play_sound_at(sndShot, throwing.x, throwing.y, 0, 100, 300, 1, false, 1);
        throwing.weapon = tWeapon;
        throwing.xMovement = xMove;
        throwing.yMovement = yMove;
        throwing.attackFacing = tFacing;
        throwing.throwingID = tID;
        break;
    // Destroy the throwing attack
    case MSG_DESTROY_THROWING:
        var tID = buffer_read(bufferClientReceived, buffer_u32);
        var tNumber = instance_number(objThrowingWeapon);
        show_debug_message("ENTERED DESTROY THROWING");
        show_debug_message("TID: " + string(tID));
        for (var i = 0; i < tNumber; i++) {
            var throwing = instance_find(objThrowingWeapon, i);
            show_debug_message("THROWING ID: " + string(throwing.throwingID));
            if (throwing.throwingID == tID) {
                with(throwing) {
                    instance_destroy();
                }
                break;
            }
        }
        break;
    case MSG_UPDATE_THROWING:
        var tX = buffer_read(bufferClientReceived, buffer_f32);
        var tY = buffer_read(bufferClientReceived, buffer_f32);
        var tID = buffer_read(bufferClientReceived, buffer_u32);
        var tNumber = instance_number(objThrowingWeapon);
        for (var i = 0; i < tNumber; i++) {
            var throwing = instance_find(objThrowingWeapon, i);
            if (throwing.throwingID == tID) {
                with(throwing) {
                    x = tX;
                    y = tY;
                }
                break;
            }
        }
        break;
    case MSG_GOT_POWER:
        var powerNumber = buffer_read(bufferClientReceived, buffer_u8);
        var myPlayer = instance_find(objPlayer, 0);
        switch(powerNumber){
            case 0:
                myPlayer.climbPower = true;
                myPlayer.alarm[1] = 1;
                myPlayer.alarm[6] = global.deltaMultiplier * delta_time * 60 * 30;
                break;
            case 1:
                myPlayer.jumpPower = true;
                myPlayer.alarm[1] = 1;
                myPlayer.alarm[7] = global.deltaMultiplier * delta_time * 60 * 30;
                break;
        }
        break;
    case MSG_CREATE_ITEM:
        var itemID = buffer_read(bufferClientReceived, buffer_u32);
        var itemX = buffer_read(bufferClientReceived, buffer_f32);
        var itemY = buffer_read(bufferClientReceived, buffer_f32);
        var powerNumber = buffer_read(bufferClientReceived, buffer_u8);
        var item = instance_create(itemX, itemY, objItem);
        item.itemID = itemID;
        item.powerNumber = powerNumber;
        break;
    case MSG_DESTROY_ITEM:
        var itemID = buffer_read(bufferClientReceived, buffer_u32);
        for(var i = 0; i < instance_number(objItem); i++) {
            var item = instance_find(objItem, i);
            if(item.itemID == itemID) {
                with(item){
                    instance_destroy();
                }
                break;
            }
        }
        break;
    case MSG_LOBBY_READY:
        var otherPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var otherPlayerReady = buffer_read(bufferClientReceived, buffer_bool);
        if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
            var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
            with(otherPlayer){
                isReady = otherPlayerReady;
            }
        }
        break;
    case MSG_SCORE:
        var otherPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var otherPlayerScore = buffer_read(bufferClientReceived, buffer_s32);
        if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
            var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
            with(otherPlayer){
                playerKills = otherPlayerScore;
            }
        }
        break;
    // Update the ammo
    case MSG_AMMO:
        var ammo = buffer_read(bufferClientReceived, buffer_u8);
        var myPlayer = instance_find(objPlayer, 0);
        myPlayer.ammo = ammo;
        break;
}
