///ChangePlayerCharacter(playerID, character, playerName)
//This here is to change the sprite of the player character

var otherPlayerID = argument[0];
var otherPlayerCharacter = argument[1];
var otherPlayerName = argument[2];

if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
    var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
    with(otherPlayer){
        character = otherPlayerCharacter;
        name = otherPlayerName;
    }
}
