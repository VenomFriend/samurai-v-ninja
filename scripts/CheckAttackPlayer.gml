///CheckAttackPlayer();
// Called by script AttackPlayer, which is called by objPlayer

// Server check if a player killed another one(or more than one)
// then send the dead state to everyone
// if the server is the one who died, set the respawn to 3 seconds
if(global.isHost) {
    for(var i = 0; i < instance_number(objPlayer); i++){
        var otherPlayer = instance_find(objPlayer, i);
        if(otherPlayer.id != id and place_meeting(x, y, otherPlayer) and otherPlayer.playerState != state.dead) {
            playerKills++; // add 1 to the score of the player
            with(otherPlayer) {
                playerState = state.dead;
                if(otherPlayer.isPlayer) { // If is the server's player, set the respawn
                    alarm[2] = global.deltaMultiplier * delta_time * 3 * 60;
                }
                ServerSendDeath(playerID, playerState);
                ServerSendPosition(playerID, x, y, facing, playerAngle);
                ammo = 5;
                ServerSendAmmo(playerID, ammo); // Resets the player's bullets
            }
            ServerSendScore(playerID, playerKills);           
        }
    }
}

                        

