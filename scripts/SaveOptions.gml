ini_open("options.ini");

ini_write_real("kb_controls", "up", global.KEY_UP);
ini_write_real("kb_controls", "down", global.KEY_DOWN);
ini_write_real("kb_controls", "left", global.KEY_LEFT);
ini_write_real("kb_controls", "right", global.KEY_RIGHT);
ini_write_real("kb_controls", "jump", global.KEY_JUMP);
ini_write_real("kb_controls", "attack", global.KEY_ATTACK);
ini_write_real("kb_controls", "shoot", global.KEY_SHOOT);
ini_write_real("kb_controls", "teleport", global.KEY_TELEPORT);

ini_write_string("connect", "ip", global.ipAddress);
ini_write_real("connect", "port", global.port);

ini_write_real("character", "char", global.char);

ini_close();
