///ServerSendThrowingPosition(x, y, id)

var xAtt = argument[0];
var yAtt = argument[1];
var attackID = argument[2];

//The number of sockets = the number of players connected
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_THROWING);
buffer_write(global.bufferServerWrite, buffer_f32, xAtt);
buffer_write(global.bufferServerWrite, buffer_f32, yAtt);
buffer_write(global.bufferServerWrite, buffer_u32, attackID);

for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
