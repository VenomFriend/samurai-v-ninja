///CheckCharacter(direction)
// Checks if the selected character wasn't already selected by another person
// and changes it
var myPlayer = instance_find(objPlayer, 0);
var dir = argument[0];
var loop = true;

while(loop) {
    loop = false;
    for(var i = 1; i < instance_number(objPlayer); i++) {
        var otherPlayer = instance_find(objPlayer, i);
        if(otherPlayer.character == global.char) {
            loop = true;
            if(dir == -1) {
                if(global.char > 0) {
                    global.char--;
                } else {
                    global.char = global.charCount;
                }
            } else {
                if(global.char < global.charCount) {
                    global.char++;
                } else {
                    global.char = 0;
                }
            }
        }
    }
}
