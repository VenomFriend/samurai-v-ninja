//ClientSendKey(key)

var key = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_KEY);
buffer_write(global.bufferClientWrite, buffer_u16, key);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
