///ChangePlayerState(playerID, state)
//This here is to change the state of the player(maybe to dead, or any other state)

var otherPlayerID = argument[0];
var otherPlayerState = argument[1];

if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
    var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
    with(otherPlayer){
        playerState = otherPlayerState;
        if(playerState == state.dead and otherPlayer.isPlayer) {
            alarm[2] = 3 * 60 * global.deltaMultiplier * delta_time; // set respawn
        }
    }
}
