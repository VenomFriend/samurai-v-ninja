///ServerSendDestroyItem(itemID)
var itemID = argument[0];

var socketSize = ds_list_size(global.socketList);
// Send the item to everyone

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_DESTROY_ITEM);
buffer_write(global.bufferServerWrite, buffer_u32, itemID);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
