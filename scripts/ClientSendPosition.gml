///ClientSendPosition(x, y, facing, angle)
var playerX = argument[0];
var playerY = argument[1];
var playerFacing = argument[2];
var playerAngle = argument[3];
//From times to times, the client will send his position to the server
//just to make sure that it's all good

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_POSITION);
buffer_write(global.bufferClientWrite, buffer_f32, playerX);
buffer_write(global.bufferClientWrite, buffer_f32, playerY);
buffer_write(global.bufferClientWrite, buffer_s8, playerFacing);
buffer_write(global.bufferClientWrite, buffer_s16, playerAngle);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
