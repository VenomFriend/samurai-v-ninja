///UpdateOtherPlayer(playerID, key, playerX, playerY, facing, playerAngle)
//This here is to update player's x, y, facing direction and angle

var otherPlayerID = argument[0];
var otherPlayerX = argument[1];
var otherPlayerY = argument[2];
var otherPlayerFacing = argument[3];
var otherPlayerAngle = argument[4];

if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
    var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
    with(otherPlayer){
        x = otherPlayerX;
        y = otherPlayerY;
        facing = otherPlayerFacing;
        playerAngle = otherPlayerAngle;
    }
}
