///ServerSendCharacter(socket, character, name)
var playerSocket = argument[0];
var playerCharacter = argument[1];
var playerName = argument[2];


var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CHARACTER);
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);
buffer_write(global.bufferServerWrite, buffer_u8, playerCharacter);
buffer_write(global.bufferServerWrite, buffer_string, playerName);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if(thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
