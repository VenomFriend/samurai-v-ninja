///HoldAttack(upside)
// Called by objPlayer
var upside = argument[0];
if(upside) {
    if(keyLeft){
        facing = 1;
    } else if(keyRight) {
        facing = -1;
    }
} else {
    if(keyLeft){
        facing = -1;
    } else if(keyRight) {
        facing = 1;
    }
}

if(canFall) {
    if (vspd < vspdMax) {
        vspd += grav;
        if (vspd > vspdMax) {
            vspd = vspdMax;
        }
    }
}

if(place_meeting(x, y+vspd, objCollision)) {
    while(!place_meeting(x, y+sign(vspd), objCollision)){
        y += sign(vspd);
    }
    vspd = 0;
    canFall = false;
    alarm[5] = global.deltaMultiplier * delta_time * 60; // 1 second to be able to fall again
}
y += vspd;
