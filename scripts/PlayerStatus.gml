///PlayerStatus()
// Called by objPlayer

name = "";
playerSpeed = 6;
attackSpeed = 80;
teleportDistance = 150;
jumpSpeed = -8;
playerID = 0; // The player ID is gonna be his Socket on the Server

playerAngle = 0; // Rotate the player in some powers

grav = 0.25;
hspd = 0;
vspd = 0;
vspdMax = 10;

facing = 1;
isPlayer = false;

// actions/states
playerState = state.idle; 
// the states are: idle, moving(on floor), jumping, falling, attacking, holding attack and dead... I 
//put it like that, 'cause when you're in one of those states, you are not in the other
//so it's easier than putting lots of variables for each, like "idle = true, moving = true" and so on
//since then I would have lots of variables, with only one being true all the time

canAttack = true;
canTeleport = true;
canShoot = true;
canFall = true; // used when holding attack in the air

character = 2; // 0 = samurai, 1 = ninja, 2 = ranger
playerIdleSprite = samuraiIdle;
playerMovementSprite = samuraiMovement;
playerJumpSprite = samuraiJump;
playerAttackSprite = samuraiAttack;
playerDeadSprite = samuraiDead;


//POWERS
climbPower = false;
wallWalking = false;
ceilingWalking = false;
wallDirection = -1; // -1 = left, 1 = right

jumpPower = false;
doubleJumped = false;

// LOBBY STUFF
isReady = false;

// Respawn Stuff
respawnX = 0;
respawnY = 0;

// Mauricio is bitching for me to do this shit
playerKills = 0;

ammo = 0; // how many bullets you have. The SERVER is the one who updates it for you

// Initialize the controls
ResetKeys();
