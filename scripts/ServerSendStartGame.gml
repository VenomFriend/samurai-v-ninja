///ServerSendStartGame(map, mapPos)
var map = argument[0];
var mapPos = argument[1];



var socketSize = ds_list_size(global.socketList);
for(var i = 0; i<socketSize; i++) {
    buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
    buffer_write(global.bufferServerWrite, buffer_u8, MSG_START);
    buffer_write(global.bufferServerWrite, buffer_u32, map);
    buffer_write(global.bufferServerWrite, buffer_f32, mapPos[i+1, 0]);
    buffer_write(global.bufferServerWrite, buffer_f32, mapPos[i+1, 1]);
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
