///ClientSendCharacter(character, name)

var character = argument[0];
var name = argument[1];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_CHARACTER);
buffer_write(global.bufferClientWrite, buffer_u8, character);
buffer_write(global.bufferClientWrite, buffer_string, name);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
