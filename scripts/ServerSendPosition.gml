///ServerSendPosition(playerID, x, y, facing, angle)
var otherPlayerID = argument[0];
var playerX = argument[1];
var playerY = argument[2];
var playerFacing = argument[3];
var playerAngle = argument[4];

var socketSize = ds_list_size(global.socketList);
// Send the position to everyone

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_POSITION);
buffer_write(global.bufferServerWrite, buffer_u32, otherPlayerID);
buffer_write(global.bufferServerWrite, buffer_f32, playerX);
buffer_write(global.bufferServerWrite, buffer_f32, playerY);
buffer_write(global.bufferServerWrite, buffer_s8, playerFacing);
buffer_write(global.bufferServerWrite, buffer_s16, playerAngle);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if(thisSocket != otherPlayerID) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
