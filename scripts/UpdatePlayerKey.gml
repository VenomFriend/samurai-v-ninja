///UpdatePlayerKey(key)
//What this does is to get the key received and change the value on the player
var key = argument[0];
switch(key) {
    case KLP:
        keyLeft = 1;
        break;
    case KRP:
        keyRight = 1;
        break;
    case KUP:
        keyUp = 1;
        break;
    case KDP:
        keyDown = 1;
        break;
    case KAP:
        keyAttack = 1;
        keyAttackPressed = 1;
        break;
    case KJP:
        keyJumpPressed = 1;
        break;
    case KLR:
        keyLeft = 0;
        break;
    case KRR:
        keyRight = 0;
        break;
    case KUR:
        keyUp = 0;
        break;
    case KDR:
        keyDown = 0;
        break;
    case KAR:
        keyAttack = 0;
        keyAttackPressed = 0;
        if(canAttack) {
            keyAttackReleased = 1;
        } else {
            keyAttackReleased = 0;
        }
        break;
    case KTP:
        keyTeleportPressed = 1;
        break;
    case KSP:
        keyShootPressed = 1;
        break;
}
