///DrawPlayer()
// Called by objPlayer

var delta = 60/1000000 * delta_time;

switch(character){
    case 0:
        playerIdleSprite = samuraiIdle;
        playerMovementSprite = samuraiMovement;
        playerJumpSprite = samuraiJump;
        playerAttackSprite = samuraiAttack;
        playerDeadSprite = samuraiDead;
        break;
    case 1:
        playerIdleSprite = ninjaIdle;
        playerMovementSprite = ninjaMovement;
        playerJumpSprite = ninjaJump;
        playerAttackSprite = ninjaAttack;
        playerDeadSprite = ninjaDead;
        break;
    case 2:
        playerIdleSprite = rangerIdle;
        playerMovementSprite = rangerMovement;
        playerJumpSprite = rangerJump;
        playerAttackSprite = rangerAttack;
        playerDeadSprite = rangerDead;
        break;
    case 3:
        playerIdleSprite = pirateIdle;
        playerMovementSprite = pirateMovement;
        playerJumpSprite = pirateJump;
        playerAttackSprite = pirateAttack;
        playerDeadSprite = pirateDead;
        break;
    case 4:
        playerIdleSprite = vikingIdle;
        playerMovementSprite = vikingMovement;
        playerJumpSprite = vikingJump;
        playerAttackSprite = vikingAttack;
        playerDeadSprite = vikingDead;
        break;
    case 5:
        playerIdleSprite = baldyIdle;
        playerMovementSprite = baldyMovement;
        playerJumpSprite = baldyJump;
        playerAttackSprite = baldyAttack;
        playerDeadSprite = baldyDead;
        break;
}
draw_self();

switch(playerState){
    case state.idle:
        image_speed = 0.125 * delta;
        sprite_index = playerIdleSprite;
        image_xscale = facing;
        image_angle = playerAngle;
        break;
    case state.moving:
        image_speed = 0.25  * delta;;
        sprite_index = playerMovementSprite;
        image_xscale = facing;
        image_angle = playerAngle;
        break;
    case state.jumping:
        sprite_index = playerJumpSprite;
        image_xscale = facing;
        image_speed = 0;
        image_index = 0;
        image_angle = playerAngle;
        break;
    case state.falling:
        sprite_index = playerJumpSprite;
        image_xscale = facing;
        image_speed = 0;
        image_index = 1;
        image_angle = playerAngle;
        break;
    case state.holdingAttack:
        image_speed = 0;
        image_index = 0;
        sprite_index = playerAttackSprite;
        image_xscale = facing;
        image_angle = playerAngle;
        break;
    case state.attacking:
        image_speed = 0;
        image_index = 1;
        sprite_index = playerAttackSprite;
        image_xscale = facing;
        image_angle = playerAngle;
        break;
    case state.dead:
        image_speed = 0;
        image_index = 0;
        sprite_index = playerDeadSprite;
        image_angle = playerAngle;
        break;
    case state.throwing:
        image_speed = 0;
        image_index = 2;
        sprite_index = playerAttackSprite;
        image_xscale = facing;
        image_angle = playerAngle;
        break;
    case state.wallSliding:
        sprite_index = playerJumpSprite;
        image_xscale = facing;
        image_speed = 0;
        image_index = 2;
        image_angle = playerAngle;
        break;
}

if(global.showName) {
    draw_set_halign(fa_center);
    draw_text(x, y-50, name);
}

// Show Collision Box
if(global.debug) {
    draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, true);
    
    draw_circle_colour(x, y, 3, c_red, c_red, false);
}
