///ServerSendDisconnect(socket)
var playerDisconnected = argument[0];
var socketSize = ds_list_size(global.socketList);

// Tell everyone to delete the player who disconnected
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_DISCONNECT_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u32, playerDisconnected);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
