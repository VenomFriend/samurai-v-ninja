///ChangePlayerStatus(playerID, canTeleport, canAttack, canShoot, canFall, climbPower, wallWalking, ceilingWalking, wallDirection, jumpPower, doubleJumped)
//This here is to change the cans and can'ts of the player
//For now, there's just a canTeleport
//(FUTURE) DEAR GOD, WHY?

var otherPlayerID = argument[0];
var otherCanTeleport = argument[1];
var otherCanAttack = argument[2];
var otherCanShoot = argument[3];
var otherCanFall = argument[4];
var otherClimbPower = argument[5];
var otherWallWalking = argument[6];
var otherCeilingWalking = argument[7];
var otherWallDirection = argument[8];
var otherJumpPower = argument[9];
var otherDoubleJumped = argument[10];

if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
    var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
    with(otherPlayer){
        canTeleport = otherCanTeleport;
        canAttack = otherCanAttack;
        canShoot = otherCanShoot;
        canFall = otherCanFall;
        climbPower = otherClimbPower;
        wallWalking = otherWallWalking;
        ceilingWalking = otherCeilingWalking;
        wallDirection = otherWallDirection;
        jumpPower = otherJumpPower;
        doubleJumped = otherDoubleJumped;
    }
}
