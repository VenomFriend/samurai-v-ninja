///ServerSendCreateItem(id, x, y, powerNumber)
var itemID = argument[0];
var itemX = argument[1];
var itemY = argument[2];
var powerNumber = argument[3];

var socketSize = ds_list_size(global.socketList);
// Send the item to everyone

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_ITEM);
buffer_write(global.bufferServerWrite, buffer_u32, itemID);
buffer_write(global.bufferServerWrite, buffer_f32, itemX);
buffer_write(global.bufferServerWrite, buffer_f32, itemY);
buffer_write(global.bufferServerWrite, buffer_u8, powerNumber);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
