///ServerSendDestroyThrowing(id)
var throwingID = argument[0];


var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_DESTROY_THROWING);
buffer_write(global.bufferServerWrite, buffer_u32, throwingID);

for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
