///ServerReceivedPacket(buffer, socket)
var bufferServerReceived = argument[0];
var socket = argument[1];
var msgid = buffer_read(bufferServerReceived, buffer_u8);

switch (msgid) {
    // receives the key press/release from the client, and send to everybody
    // also receives their x and y, just to make sure it's updated
    case MSG_KEY:
        var key = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))) {
            ServerSendKey(key, socket);
            // Remember that we stored the Instance on a MAP with the SOCKET as the key
            var inst = ds_map_find_value(global.Clients, socket);
            with(inst) {
                UpdatePlayerKey(key);
            }
        }
        break;
    // receive the x and y of the player from time to time
    case MSG_POSITION:
        var otherPlayerX = buffer_read(bufferServerReceived, buffer_f32);
        var otherPlayerY = buffer_read(bufferServerReceived, buffer_f32);
        var otherPlayerFacing = buffer_read(bufferServerReceived, buffer_s8);
        var otherPlayerAngle = buffer_read(bufferServerReceived, buffer_s16);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))) {
            // Remember that we stored the Instance on a MAP with the SOCKET as the key
            var inst = ds_map_find_value(global.Clients, socket);
            if (inst.x != otherPlayerX or inst.y != otherPlayerY) {
                inst.x = otherPlayerX;
                inst.y = otherPlayerY;
                inst.facing = otherPlayerFacing;
                inst.playerAngle = otherPlayerAngle;
                ServerSendPosition(socket, otherPlayerX, otherPlayerY, otherPlayerFacing, otherPlayerAngle);
            }
            
        }
        break;
    // Change the player state(dead? alive? etc)
    case MSG_STATE:
        var otherPlayerState = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))) {
            var inst = ds_map_find_value(global.Clients, socket);
            inst.playerState = otherPlayerState;
            ServerSendState(socket, otherPlayerState);
        }
        break;
    // Change the player status(can teleport?)
    case MSG_STATUS:
        var otherPlayerTeleport = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerAttack = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerShoot = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerFall = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerClimbPower = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerWallWalking = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerCeilingWalking = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerWallDirection = buffer_read(bufferServerReceived, buffer_s8);
        var otherPlayerJumpPower = buffer_read(bufferServerReceived, buffer_bool);
        var otherPlayerDoubleJumped = buffer_read(bufferServerReceived, buffer_bool);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))) {
            var inst = ds_map_find_value(global.Clients, socket);
            inst.canTeleport = otherPlayerTeleport;
            inst.canAttack = otherPlayerAttack;
            inst.canShoot = otherPlayerShoot;
            inst.canFall = otherPlayerFall;
            inst.climbPower = otherPlayerClimbPower;
            inst.wallWalking = otherPlayerWallWalking;
            inst.ceilingWalking = otherPlayerCeilingWalking;
            inst.wallDirection = otherPlayerWallDirection;
            inst.jumpPower = otherPlayerJumpPower;
            inst.doubleJumped = otherPlayerDoubleJumped;
            ServerSendStatus(socket, inst);
        }
        break;
    // Change the player character and update everybody
    case MSG_CHARACTER:
        var otherPlayerCharacter = buffer_read(bufferServerReceived, buffer_u8);
        var otherPlayerName = buffer_read(bufferServerReceived, buffer_string);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))) {
            var inst = ds_map_find_value(global.Clients, socket);
            inst.character = otherPlayerCharacter;
            inst.name = otherPlayerName;
            ServerSendCharacter(socket, otherPlayerCharacter, otherPlayerName);
        }
        break;
    case MSG_LOBBY_READY:
        var otherPlayerIsReady = buffer_read(bufferServerReceived, buffer_bool);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))) {
            var inst = ds_map_find_value(global.Clients, socket);
            inst.isReady = otherPlayerIsReady;
            ServerSendLobbyReady(socket, otherPlayerIsReady);
        }
        break;
}
