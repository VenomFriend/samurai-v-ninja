///ViewFollowPlayer()

if !view_enabled and room != roomMenu and room != roomLobby {
    view_visible[0] = true;
    view_enabled = true;
    view_hview[0] = 480;
    view_wview[0] = 640;
}
var myPlayer = instance_find(objPlayer, 0);
if(myPlayer != noone) {
    if(myPlayer.x-view_wview[0]/2 < 0) {
        view_xview[0] = 0;
    } else if (myPlayer.x-view_wview[0]/2 > room_width - view_wview[0]){
        view_xview[0] = room_width - view_wview[0];
    } else {
        view_xview[0] = myPlayer.x-view_wview[0]/2;
    }
    if(myPlayer.y-view_hview[0]/2 < 0) {
        view_yview[0] = 0;
    } else if (myPlayer.y-view_hview[0]/2 > room_height - view_hview[0]){
        view_yview[0] = room_height - view_hview[0];
    } else {
        view_yview[0] = myPlayer.y-view_hview[0]/2;
    }
}
