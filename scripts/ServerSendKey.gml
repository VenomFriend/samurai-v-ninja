///ServerSendKey(key, playerID)
var key = argument[0];
var otherPlayerID = argument[1];

var socketSize = ds_list_size(global.socketList);
// Send the key to everyone

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_KEY);
buffer_write(global.bufferServerWrite, buffer_u32, otherPlayerID);
buffer_write(global.bufferServerWrite, buffer_u8, key);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if(thisSocket != otherPlayerID) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
