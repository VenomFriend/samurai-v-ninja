///ServerPlayerDisconnect(socket, inst);

var socket = argument[0];
var inst = argument[1];

//Delete it from the map
ds_map_delete(global.Clients, socket);
//Tell everybody to delete this player
ServerSendDisconnect(socket);

// And delete the instance of the player from the server
with (inst) {
    show_debug_message("INSIDE INST IF");
    show_debug_message(inst);
    instance_destroy();
}


var findSocket = ds_list_find_index(global.socketList, socket);
if (findSocket >= 0) {
    ds_list_delete(global.socketList, findSocket);
}
