///ServerSendScore(playerID, score)
var otherPlayerID = argument[0];
var otherPlayerScore = argument[1];

var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_SCORE);
buffer_write(global.bufferServerWrite, buffer_u32, otherPlayerID);
buffer_write(global.bufferServerWrite, buffer_s32, otherPlayerScore);


for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
