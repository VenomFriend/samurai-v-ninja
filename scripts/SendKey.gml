///SendKey()
if(isPlayer) { // Shouldn't enter here otherwise, but just to inforce it
    if(global.isHost) {
        // keys pressed
        // KLP means Key Left Pressed, KLR means Key Left Released
        // All the other names follows this same rule
        // Here, the server is sending his own key presses to the clients, so I can 
        // use his playerID
        // When he sends other players presses, he does so when he receives them, on 
        // ServerReceivedPacket
        
        if(keyLeftPressed) {
            ServerSendKey(KLP, playerID, x, y);
        }
        if(keyRightPressed) {
            ServerSendKey(KRP, playerID, x, y);
        }
        if(keyUpPressed) {
            ServerSendKey(KUP, playerID, x, y);
        }
        if(keyDownPressed) {
            ServerSendKey(KDP, playerID, x, y);
        }
        if(keyAttackPressed) {
            ServerSendKey(KAP, playerID, x, y);
        }
        if(keyJumpPressed) {
            ServerSendKey(KJP, playerID, x, y);
        }
        if(keyTeleportPressed){
            ServerSendKey(KTP, playerID, x, y);
        }
        if(keyShootPressed){
            ServerSendKey(KSP, playerID, x, y);
        }
        
        // keys released
        if(keyLeftReleased) {
            ServerSendKey(KLR, playerID, x, y);
        }
        if(keyRightReleased) {
            ServerSendKey(KRR, playerID, x, y);
        }
        if(keyUpReleased) {
            ServerSendKey(KUR, playerID, x, y);
        }
        if(keyDownReleased) {
            ServerSendKey(KDR, playerID, x, y);
        }
        if(keyAttackReleased) {
            ServerSendKey(KAR, playerID, x, y);
        }
        if(keyJumpReleased) {
            ServerSendKey(KJR, playerID, x, y);
        }
    } else {
    
        // keys pressed
        
        //Client doesn't need to send an ID, since his ID is his SOCKET, and the 
        //Server will know his socket when he send the message
        if(keyLeftPressed) {
            ClientSendKey(KLP, x, y);
        }
        if(keyRightPressed) {
            ClientSendKey(KRP, x, y);
        }
        if(keyUpPressed) {
            ClientSendKey(KUP, x, y);
        }
        if(keyDownPressed) {
            ClientSendKey(KDP, x, y);
        }
        if(keyAttackPressed) {
            ClientSendKey(KAP, x, y);
        }
        if(keyJumpPressed) {
            ClientSendKey(KJP, x, y);
        }
        if(keyTeleportPressed){
            ClientSendKey(KTP, x, y);
        }
        if(keyShootPressed){
            ClientSendKey(KSP, x, y);
        }
        
        // keys released
        if(keyLeftReleased) {
            ClientSendKey(KLR, x, y);
        }
        if(keyRightReleased) {
            ClientSendKey(KRR, x, y);
        }
        if(keyUpReleased) {
            ClientSendKey(KUR, x, y);
        }
        if(keyDownReleased) {
            ClientSendKey(KDR, x, y);
        }
        if(keyAttackReleased) {
            ClientSendKey(KAR, x, y);
        }
        if(keyJumpReleased) {
            ClientSendKey(KJR, x, y);
        }
        
    }
}
