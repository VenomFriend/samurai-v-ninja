///ServerSendError(socket, msg)
var socket = argument[0];
var message = argument[1];

// Tell the player who just connected his ID and position
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_ERROR);
buffer_write(global.bufferServerWrite, buffer_string, message);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
