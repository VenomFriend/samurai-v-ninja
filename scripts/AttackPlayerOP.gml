canAttack = false;
playerState = state.attacking;
var doEffect = 5;
if(keyUp) {
    if(keyLeft){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x-1, y-1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x-=1;
                y-=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
    }
    else if(keyRight){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x+1, y-1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x+=1;
                y-=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
    }
    else{
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x, y-1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                y-=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
    }
}
else if(keyDown) {
    if(keyLeft){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x-1, y+1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x-=1;
                y+=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
    }
    else if(keyRight){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x+1, y+1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x+=1;
                y+=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
    }
    else{
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x, y+1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                y+=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
    }
}
else if(keyLeft or facing == -1) {
    for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x-1, y, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x-=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
} 
else if (keyRight or facing == 1) {
    for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x+1, y, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, 1, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x+=1;
                if CheckAttackPlayerOP() {
                    break;
                }
            } else {
                break;
            }
        }
}
alarm[0] = room_speed;
