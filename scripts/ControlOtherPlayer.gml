///ControlOtherPlayer(playerID, key)
//This here is to control the other players in your game, based on the keys you received
//from the server

var otherPlayerID = argument[0];
var key = argument[1];

if (!is_undefined(ds_map_find_value(global.players, otherPlayerID))){
    var otherPlayer = ds_map_find_value(global.players, otherPlayerID);
    with(otherPlayer){
        UpdatePlayerKey(key);
    }
}
