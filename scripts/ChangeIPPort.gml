///ChangeIPPort(optionSelected)

switch(optionSelected) {
    case 0:
        if (string_width(keyboard_string) < 360) {
            global.ipAddress = keyboard_string;
        } else {
            keyboard_string = global.ipAddress;
        }
        break;
    case 1:
        if (string_width(keyboard_string) < 360) {
            global.port = real(keyboard_string);
        } else {
            keyboard_string = global.port;
        }
        break;
    case 2:
        if (string_width(keyboard_string) < 360) {
            global.name = keyboard_string;
        } else {
            keyboard_string = global.name;
        }
        break;
}
