///ServerSendNewPlayer(socket, x, y, playerFacing, playerCharacter, playerName, playerIsReady)

var playerSocket = argument[0];
var playerX = argument[1];
var playerY = argument[2];
var playerFacing = argument[3];
var playerCharacter = argument[4];
var playerName = argument[5];
var playerIsReady = argument[6];

//The number of sockets = the number of players connected
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u16, 1); // Only creating one player
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);  //Remember, kids, your ID is your socket, not your player.id!
buffer_write(global.bufferServerWrite, buffer_f32, playerX);
buffer_write(global.bufferServerWrite, buffer_f32, playerY);
buffer_write(global.bufferServerWrite, buffer_s8, playerFacing);
buffer_write(global.bufferServerWrite, buffer_u8, playerCharacter);
buffer_write(global.bufferServerWrite, buffer_string, playerName);
buffer_write(global.bufferServerWrite, buffer_bool, playerIsReady);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if(thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
