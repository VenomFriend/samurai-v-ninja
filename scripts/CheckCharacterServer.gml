///CheckCharacterServer(char)
// Checks if the selected character wasn't already selected by another person
// and changes it
var loop = true;
var char = 0;
while(loop) {
    loop = false;
    for(var i = 0; i < instance_number(objPlayer); i++) {
        var otherPlayer = instance_find(objPlayer, i);
        if(otherPlayer.character == char) {
            loop = true;
            if(char < global.charCount) {
                char++;
            } else {
                char = 0;
            }
        }
    }
}
return char; 
