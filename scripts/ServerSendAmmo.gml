///ServerSendAmmo(socket, ammo)

//Tells the player how much ammo he has

var socket = argument[0];
var ammo = argument[1];

// Tell the player who just connected his ID and position
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_AMMO);
buffer_write(global.bufferServerWrite, buffer_u8, ammo);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
