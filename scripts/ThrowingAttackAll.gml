///ThrowingAttackAll(upside)
// called by objPlayer
var upside = argument[0];
var trueFacing = facing;
if(upside) {
    trueFacing = -trueFacing;
}

vspd = 0;
var xMovement = 0;
var yMovement = 0;
var attackFacing = -1;
var diagonalSpeed = 1/sqrt(2);
var xToAdd = 0;
var yToAdd = 0;

if(climbPower and wallWalking) {
    // if you are wall walking, the default direction should be UP or DOWN, depending where you are facing
    if(keyLeft){// and wallDirection != -1){
        xToAdd = -20;
        xMovement = -1;
        attackFacing = 1;
    }
    else if(keyRight){// and wallDirection != 1){
        xToAdd = 20;
        xMovement = 1;
        attackFacing = 0;
    }
    else if(keyUp or (wallDirection == -1 and facing == -1) or (wallDirection == 1 and facing == 1)) {
        yMovement = -1;
        yToAdd = -30;
        attackFacing = 2;
        // Don't shoot left if you are on a wall to the left
        if(keyLeft){
            xToAdd = -20;
            xMovement = -diagonalSpeed;
            yMovement = -diagonalSpeed;
            attackFacing = 6;
        } 
        else if (keyRight){
            xToAdd = 20;
            xMovement = diagonalSpeed;
            yMovement = -diagonalSpeed;
            attackFacing = 4;
        } 
    }
    else if(keyDown or (wallDirection == -1 and facing == 1) or (wallDirection == 1 and facing == -1)) {
        yToAdd = 35;
        yMovement = 1;
        attackFacing = 3;
        if(keyLeft){
            xToAdd = -20;
            xMovement = -diagonalSpeed;
            yMovement = diagonalSpeed;
            attackFacing = 7;
        } 
        else if (keyRight){
            xToAdd = 20;
            xMovement = diagonalSpeed;
            yMovement = diagonalSpeed;
            attackFacing = 5;
        } 
    }
} else {
    if(keyUp) {
        yMovement = -1;
        yToAdd = -30;
        attackFacing = 2;
        if(keyLeft){
            xToAdd = -20;
            xMovement = -diagonalSpeed;
            yMovement = -diagonalSpeed;
            attackFacing = 6;
        } 
        else if (keyRight) {
            xToAdd = 20;
            xMovement = diagonalSpeed;
            yMovement = -diagonalSpeed;
            attackFacing = 4;
        } 
    }
    else if(keyDown){
        yToAdd = 35;
        yMovement = 1;
        attackFacing = 3;
        if(keyLeft){
            xToAdd = -20;
            xMovement = -diagonalSpeed;
            yMovement = diagonalSpeed;
            attackFacing = 7;
        } 
        else if (keyRight) {
            xToAdd = 20;
            xMovement = diagonalSpeed;
            yMovement = diagonalSpeed;
            attackFacing = 5;
        } 
    }
    else if(keyLeft or trueFacing == -1) {
        xToAdd = -20;
        xMovement = -1;
        attackFacing = 1;
    }
    else if(keyRight or trueFacing == 1) {
        xToAdd = 20;
        xMovement = 1;
        attackFacing = 0;
    }
}
if(global.isHost and ammo > 0) { // shouldn't enter here otherwise
    var throwingAttack = instance_create(x + xToAdd, y + yToAdd, objThrowingWeapon);
    audio_play_sound_at(sndShot, throwingAttack.x, throwingAttack.y, 0, 100, 300, 1, false, 1);
    switch(character){
        case 0: // samurai
            throwingAttack.weapon = 1;
            break;
        case 1: // ninja
            throwingAttack.weapon = 0;
            break;
        case 2: // ranger
            throwingAttack.weapon = 2;
            break;
        case 3: // Pirate
            throwingAttack.weapon = 3;
            break;
        case 4: // Viking
            throwingAttack.weapon = 4;
            break;
        case 5: // Caped Baldy
            throwingAttack.weapon = 5;
            break;
    }
    throwingAttack.xMovement = xMovement;
    throwingAttack.yMovement = yMovement;
    throwingAttack.attackFacing = attackFacing;
    throwingAttack.throwingID = throwingAttack.id;
    throwingAttack.playerID = playerID; // only the server needs to know who was the player
    ServerSendThrowing(throwingAttack.x, throwingAttack.y, throwingAttack.weapon, xMovement, yMovement, attackFacing, throwingAttack.throwingID);
    ammo--; // -1 bullet
    ServerSendAmmo(playerID, ammo); // tell the player how many bullets he has now
}
