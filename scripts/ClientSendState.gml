///ClientSendState(playerState)

var playerState = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_STATE);
buffer_write(global.bufferClientWrite, buffer_u16, playerState);
var msgSent = network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));

if(msgSent < 0) {
    show_message("THE HOST HAS DISCONNECTED.");
    game_restart();
}
