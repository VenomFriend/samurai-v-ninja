///MovePlayer()
// Called by objPlayer

var delta = 60/1000000 * delta_time;

if (vspd < vspdMax) {
    vspd += grav;
    if (vspd > vspdMax) {
        vspd = vspdMax;
    }
}


var movement = -keyLeft + keyRight;
// keyLeft is either 1(active) or 0
// keyRight is either 1(active) or 0
//so if movement = -1, it's going left, if movement = 0, it's not moving and if movement = 1, it's going right

if(movement == 1) {
    facing = 1; // facing right
} else if(movement == -1){
    facing = -1; //facing left
}

if(place_meeting(x, y+1, objCollision)){
    hspd = movement * playerSpeed;
    //If you cannot attack because you already attacked, when you are on the floor you'll be able to
    //attack again. This way, you won't be able to attack twice while on air... I guess :P
    if(!canAttack){
        canAttack = true;
    }
    if(hspd != 0) {
        playerState = state.moving;
    } else {
        playerState = state.idle;
    }
} else {
    hspd += movement * 1;
    if(abs(hspd) > abs(playerSpeed)){
        hspd = movement * playerSpeed;
    }
}

// Double jump

if(playerState == state.jumping or playerState == state.falling) {
    if(keyJumpPressed and jumpPower and !doubleJumped) {
        doubleJumped = true;
        vspd = jumpSpeed;
        playerState = state.jumping;    
        effect_create_above(ef_smoke, x+13, y+13, 0.3, c_gray);
    }
}

if(keyJumpPressed) {
    // If you are on the floor
    if(place_meeting(x, y+1, objCollision)){
        audio_play_sound_at(sndJump, x, y, 0, 100, 300, 1, false, 1);
        vspd = jumpSpeed;
        playerState = state.jumping;
    } else {
        // If there's a wall on your right
        if(place_meeting(x+1, y, objCollision)){
            audio_play_sound_at(sndJump, x, y, 0, 100, 300, 1, false, 1);
            vspd = jumpSpeed;
            hspd = -playerSpeed;
            facing = sign(hspd);
            playerState = state.jumping;
            //After a wall jump, you can attack again
            if(!canAttack){
                canAttack = true;
            }
        // If there's a wall on your left
        } else if(place_meeting(x-1, y, objCollision)){
            audio_play_sound_at(sndJump, x, y, 0, 100, 300, 1, false, 1);
            vspd = jumpSpeed;
            hspd = playerSpeed;
            facing = sign(hspd);
            playerState = state.jumping;
            //After a wall jump, you can attack again
            if(!canAttack){
                canAttack = true;
            }
        }
    }
}


if (!place_meeting(x, y+1, objCollision)) { //(vspd != 0 and !place_meeting(x, y+1, objCollision)) {
    if(place_meeting(x+1, y, objCollision) or place_meeting(x-1, y, objCollision)){
        playerState = state.wallSliding;
        if(vspd >= vspdMax/10 ) {
            vspd = vspdMax/10;
        }
    } else{
        if(vspd < 0) {
            playerState = state.jumping;
        } else {
            playerState = state.falling;
        }
    }
}



// Reset the double jump
if(playerState != state.jumping and playerState != state.falling) {
    // Only reset if you are either on the floor or touching a wall
    if(place_meeting(x+1, y, objCollision) or place_meeting(x-1, y, objCollision) or place_meeting(x, y+1, objCollision)){
        if(doubleJumped) {
            doubleJumped = false;
        }
    }
}


if(place_meeting(x + (hspd * delta), y, objCollision)) {
    while(!place_meeting(x + (sign(hspd) * delta), y, objCollision)){
        x += (sign(hspd) * delta);
    }
    hspd = 0;
}

if(place_meeting(x, y + (vspd * delta), objCollision)) {
    while(!place_meeting(x, y + (sign(vspd) * delta), objCollision)){
        y += (sign(vspd) * delta);
    }
    vspd = 0;
}



// THIS HERE IS IMPORTANT AS HELL!
if(!place_meeting(x + (hspd * delta), y, objCollision)) {
    x += (hspd * delta);
}
if(!place_meeting(x, y + (vspd * delta), objCollision)) {
    y += (vspd * delta);
}
