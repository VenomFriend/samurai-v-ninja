///ChangeKeys(key);
//The menu change keys
newKey = argument[0];
changingKey = false;
if(newKey == vk_escape or newKey == vk_enter) {
    exit;
}
switch(keySelected) {
    case 0: // UP
        global.KEY_UP = newKey;
        break;
    case 1: // DOWN
        global.KEY_DOWN = newKey;
        break;
    case 2: // LEFT
        global.KEY_LEFT = newKey;
        break;
    case 3: // RIGHT
        global.KEY_RIGHT = newKey;
        break;
    case 4: // JUMP
        global.KEY_JUMP = newKey;
        break;
    case 5: // ATTACK
        global.KEY_ATTACK = newKey;
        break;
    case 6: // SHOOT
        global.KEY_SHOOT = newKey;
        break;
    case 7: // TELEPORT
        global.KEY_TELEPORT = newKey;
        break;
}

