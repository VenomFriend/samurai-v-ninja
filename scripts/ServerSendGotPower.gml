///ServerSendGotPower(socket, power)
var socket = argument[0];
var powerNumber = argument[1];

// Tell the player who just connected his ID and position
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_GOT_POWER);
buffer_write(global.bufferServerWrite, buffer_u8, powerNumber);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
