/// Check if I've been attacked, or attacked a player that's attacking
// Returns true if found another player, false otherwise
if(place_meeting(x, y, objPlayer)) {
    var otherPlayer = instance_place(x, y, objPlayer);
    if(otherPlayer != noone) {
        if (otherPlayer.playerState == state.attacking) {
            if(playerState == state.attacking) {
                playerState = state.jumping;
                AttackCollision();
            }
        }
        return true;
    }
}
return false;
