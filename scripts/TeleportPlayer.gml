///TeleportPlayer(upside)
// Called by objPlayer

var upside = argument[0];
var trueFacing = facing;
if(upside) {
    trueFacing = -trueFacing;
}

vspd = 0;
var diagonalModifier = 0.707;
var smokeColor = c_black;
switch(character) {
    case 0: // samurai
        smokeColor = c_white;
        break;
    case 1: // ninja
        smokeColor = c_black
        break;
    case 2: // ranger
        smokeColor = c_blue;
        break;
    case 3: // pirate
        smokeColor = c_red;
        break;
    case 4: // viking
        smokeColor = c_orange;
        break;
    case 5: // baldy
        smokeColor = c_yellow;
        break;
}
effect_create_above(ef_smoke, x, y, 1, smokeColor);
if(keyUp) {
    if(keyLeft){
        for(var i = 0; i < teleportDistance; i++){
            if((place_meeting(x-diagonalModifier, y-diagonalModifier, objCollision) == false)) {
                x-= diagonalModifier; // diagonal should be less
                y-= diagonalModifier;
                if(x < 0) {
                    x = room_width;
                }
            } else {
                break;
            }
        }
        while(place_meeting(x, y, objPlayer)){
            x+=diagonalModifier;
            y+=diagonalModifier;
        }
    }
    else if(keyRight){
        for(var i = 0; i < teleportDistance; i++){
            if((place_meeting(x+diagonalModifier, y-diagonalModifier, objCollision) == false)) {
                x+= diagonalModifier;
                if(x > room_width){
                    x = 0;
                }
                y-= diagonalModifier;
            } else {
                break;
            }
        }
        while(place_meeting(x, y, objPlayer)){
            x-=diagonalModifier;
            y+=diagonalModifier;
        }
    }
    else{
        for(var i = 0; i < teleportDistance; i++){
            if((place_meeting(x, y-1, objCollision) == false)) {
                y-=1;
            } else {
                break;
            }
        }
        while(place_meeting(x, y, objPlayer)){
            y+=1;
        }
    }
}
else if(keyDown) {
    if(keyLeft){
        for(var i = 0; i < teleportDistance; i++){
            if((place_meeting(x-diagonalModifier, y+diagonalModifier, objCollision) == false)) {
                x-= diagonalModifier;
                if(x < 0) {
                    x = room_width;
                }
                y+= diagonalModifier;
            } else {
                break;
            }
        }
        while(place_meeting(x, y, objPlayer)){
            x+=diagonalModifier;
            y-=diagonalModifier;
        }
    }
    else if(keyRight){
        for(var i = 0; i < teleportDistance; i++){
            if((place_meeting(x+diagonalModifier, y+diagonalModifier, objCollision) == false)) {
                x+= diagonalModifier;
                if(x > room_width){
                    x = 0;
                }
                y+= diagonalModifier;
            } else {
                break;
            }
        }
        while(place_meeting(x, y, objPlayer)){
            x-=diagonalModifier;
            y-=diagonalModifier;
        }
    }
    else{
        for(var i = 0; i < teleportDistance; i++){
            if((place_meeting(x, y+1, objCollision) == false)) {
                y+=1;
            } else {
                break;
            }
        }
        while(place_meeting(x, y, objPlayer)){
            y-=1;
        }
    }
}
else if(keyLeft or trueFacing == -1) {
    for(var i = 0; i < teleportDistance; i++){
        if((place_meeting(x-1, y, objCollision) == false)) {
            x-=1;
            if(x < 0) {
                x = room_width;
            }
        } else {
            break;
        }
    }
    while(place_meeting(x, y, objPlayer)){
        x+=1;
    }
} 
else if (keyRight or trueFacing == 1) {
    for(var i = 0; i < teleportDistance; i++){
        if((place_meeting(x+1, y, objCollision) == false)) {
            x+=1;
            if(x > room_width){
                x = 0;
            }
        } else {
            break;
        }
    }
    while(place_meeting(x, y, objPlayer)){
        x-=1;
    }
}
effect_create_above(ef_smoke, x, y, 1, smokeColor);
