///DrawGUIPlayer()

if(room != roomLobby) {
    var player = instance_find(objPlayer, 0);
    var guiW = 640;
    var guiH = 480;
    var toAdd = 0;
    display_set_gui_size(guiW,guiH);
    draw_set_halign(fa_left);
    
    draw_sprite(sprAmmo, 0, 0, guiH - 32);
    draw_text(32, guiH - 28, string(player.ammo));
    
    if(player != noone) {
        if(player.climbPower) {
            draw_sprite(sprPower, 0, 50 + toAdd, 50);
            toAdd += 80;
        }
        
        if(player.jumpPower) {
            draw_sprite(sprPower, 1, 50 + toAdd, 50);
            toAdd += 80;
        }
    }
    if(global.showScore) {
        draw_set_halign(fa_right);
        draw_text(guiW, 0, "SCORE");
        for(var i = 0; i < instance_number(objPlayer); i++) {
            var thisPlayer = instance_find(objPlayer, i);
            draw_text(guiW, 32 + (i*32), string(thisPlayer.name) + ": " + string(thisPlayer.playerKills));
        }
    }
}
