///AttackPlayer(upside)
// Called by objPlayer
var upside = argument[0];
var trueFacing = facing;
if(upside) {
    trueFacing = -trueFacing;
}
show_debug_message("TRUE FACING: " + string(trueFacing));

vspd = 0;
var doEffect = 5;
var diagonalModifier = 1/sqrt(2);
var flareSize = 0.3;
audio_play_sound_at(sndSlash, x, y, 0, 100, 300, 1, false, 1);
if(keyUp) {
    if(keyLeft){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x-diagonalModifier, y-diagonalModifier, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x-=diagonalModifier;
                y-=diagonalModifier;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
    }
    else if(keyRight){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x+diagonalModifier, y-diagonalModifier, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x+=diagonalModifier;
                y-=diagonalModifier;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
    }
    else{
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x, y-1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                y-=1;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
    }
}
else if(keyDown) {
    if(keyLeft){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x-diagonalModifier, y+diagonalModifier, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x-=diagonalModifier;
                y+=diagonalModifier;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
    }
    else if(keyRight){
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x+diagonalModifier, y+diagonalModifier, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x+=diagonalModifier;
                y+=diagonalModifier;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
    }
    else{
        for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x, y+1, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                y+=1;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
    }
}
else if(keyLeft or trueFacing == -1) {
    for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x-1, y, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x-=1;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
} 
else if (keyRight or trueFacing == 1) {
    for(var i = 0; i < attackSpeed; i++){
            if((place_meeting(x+1, y, objCollision) == false)) {
                if(doEffect >= 5) {
                    effect_create_above(ef_flare, x, y, flareSize, c_white);
                    doEffect = 0;
                }
                doEffect++;
                x+=1;
                if(global.isHost) {
                    CheckAttackProjectile();
                    CheckAttackPlayer();
                }
            } else {
                break;
            }
        }
}
